package src;

public class Standart  extends  Room{
    private int roomNumber;
    private int nightlyRate;
    private boolean booked;

    public Standart(int roomNumber, double nightlyRate, boolean booked) {
        super(roomNumber, nightlyRate, booked);

        this.roomNumber = roomNumber;
        this.nightlyRate = (int) nightlyRate;
        this.booked = booked;
    }

    @Override
    public boolean book() {
        return false;
    }

    @Override
    public boolean checkingAvailability() {
        return false;
    }

    @Override
    public double calculatingCharges() {
        return 500;
    }
}



