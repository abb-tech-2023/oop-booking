package src;



public class Main {
    public static void main(String[] args) {

        Standart standart = new Standart(6, 0.8, true);
        Deluxe deluxe = new Deluxe(5, 0.5, true);
        Suite suite = new Suite(3, 0.4, false);
        System.out.println(deluxe.calculatingCharges());
        System.out.println(deluxe.checkingAvailability());
        System.out.println(deluxe.book());
        System.out.println(suite.calculatingCharges());
        System.out.println(suite.checkingAvailability());
        System.out.println(suite.book());
        System.out.println(standart.checkingAvailability());
        System.out.println(standart.calculatingCharges());
        System.out.println(standart.book());
    }
}

